#include <Arduino.h>
#include <Stream.h>
#include <ESP8266WiFi.h>

//AWS
#include "sha256.h"
#include "Utils.h"

//WEBSockets
#include <Hash.h>
#include <WebSocketsClient.h>

//MQTT PAHO
#include <SPI.h>
#include <IPStack.h>
#include <Countdown.h>
#include <MQTTClient.h>

//AWS MQTT Websocket
#include "Client.h"
#include "AWSWebSocketClient.h"
#include "CircularByteBuffer.h"

//JSON
#include <ArduinoJson.h>

//MP3 Module
#include <SoftwareSerial.h>
#include <DFMiniMp3.h>

//LED pins
#define RED_LED D6
#define GREEN_LED D7
#define BLUE_LED D8
#define RELAY_MOTOR D1
#define RELAY_LIGHT D0

int pinState = LOW;  //used to toggle RELAY_LIGHT pin
unsigned long previousMillis = 0;  //used as a counter for RELAY_LIGHT
int16_t speakerVolume = 0;  //Set desired speaker volume
bool muted = false;  //Enable or disable partyTime
int wifiStatus = 0;  //used when attempting a wifi connection

//AWS IOT config
char wifi_ssid[]       = "EXCELION";
char wifi_password[]   = "thevault!";
char aws_endpoint[]    = "ax6j6875rjidk.iot.us-east-1.amazonaws.com";
char aws_key[]         = "AKIAIMQOAAAIJNHLJBBA";
char aws_secret[]      = "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";
char aws_region[]      = "us-east-1";
const char* aws_get_shadow_topic             = "$aws/things/crazy-light/shadow/get";
const char* aws_get_shadow_accepted_topic    = "$aws/things/crazy-light/shadow/get/accepted";
const char* aws_update_shadow_topic          = "$aws/things/crazy-light/shadow/update";
const char* aws_update_shadow_delta_topic    = "$aws/things/crazy-light/shadow/update/delta";
const char* aws_party_time_topic             = "party-time";
int port = 443;

//MQTT config
const int maxMQTTpackageSize = 512;
const int maxMQTTMessageHandlers = 3;

//--------------------------MQTT & Websocket Setup---------------------------//
AWSWebSocketClient awsWSclient(1000);

IPStack ipstack(awsWSclient);
MQTT::Client<IPStack, Countdown, maxMQTTpackageSize, maxMQTTMessageHandlers> *client = NULL;

//# of connections
long connection = 0;

//generate random mqtt clientID
char* generateClientID () {
  char* cID = new char[23]();
  for (int i=0; i<22; i+=1)
    cID[i]=(char)random(1, 256);
  return cID;
}

//connects to websocket layer and mqtt layer
bool connect () {
  Serial.println("\nWebsocket layer connecting...");
  if (client == NULL) {
    client = new MQTT::Client<IPStack, Countdown, maxMQTTpackageSize, maxMQTTMessageHandlers>(ipstack);
  } else {
  
    if (client->isConnected ()) {    
      client->disconnect ();
    }  
    delete client;
    client = new MQTT::Client<IPStack, Countdown, maxMQTTpackageSize, maxMQTTMessageHandlers>(ipstack);
  }
  
  int rc = ipstack.connect(aws_endpoint, port);
  while (rc != 1)
  {
    errorSolidRed();
    Serial.println("Error connection to the websocket server");
    delay(1000);
  }
  Serial.println("Websocket layer connected");
  
  Serial.println("\nMQTT connecting...");     
  MQTTPacket_connectData data = MQTTPacket_connectData_initializer;
  data.MQTTVersion = 3;
  char* clientID = generateClientID ();
  data.clientID.cstring = clientID;
  rc = client->connect(data);
  delete[] clientID;
  while (rc != 0)
  {
    errorSolidRed();
    Serial.print("Error connection to MQTT server");
    Serial.println(rc);
  }
  Serial.println("MQTT connected");
  return true;
}

//subscribe to an mqtt topic
void subscribe(const char* topic, void (*messageHandler)(MQTT::MessageData&)) {
  int rc = client->subscribe(topic, MQTT::QOS0, messageHandler);
  while (rc != 0) {
    errorSolidRed();
    Serial.print("Unable to subscribe to the MQTT topic");
  }
  Serial.print("MQTT subscribed: ");
  Serial.println(topic);
}

//--------------------------MQTT Message Handlers---------------------------//
void sendMessage(const char* topic, char* msg) {
  Serial.println("\nSending a message...");
  Serial.print("  Topic: ");
  Serial.println(topic);
  Serial.print("  Payload: ");
  Serial.println(msg);
  MQTT::Message message;
  char buf[100];
  strcpy(buf, msg);
  message.qos = MQTT::QOS0;
  message.retained = false;
  message.dup = false;
  message.payload = (void*)buf;
  message.payloadlen = strlen(buf)+1;
  int rc = client->publish(topic, message); 
  Serial.println("The message was sent");
}

//update the shadow's "reported" state to confirm a successful device volume and/or muted state change
void sendUpdateSuccess(){
  char buf[45];
  sprintf(buf, "{\"state\":{\"reported\":{\"volume\":%i,\"muted\":%s}}}", speakerVolume, muted ? "true" : "false");
  sendMessage(aws_update_shadow_topic, buf);
}

//callback to handle messages from: "$aws/things/crazy-light/shadow/get/accepted"
void getShadowAcceptedMessageHandler(MQTT::MessageData &md)
{
  MQTT::Message &message = md.message;
  
  char* topic = new char[md.topicName.lenstring.len+1]();
  memcpy (topic,md.topicName.lenstring.data,md.topicName.lenstring.len);
  Serial.println("\nGet desired state message received!");
  Serial.print("  Topic: ");
  Serial.println(topic);
  
  char* msg = new char[message.payloadlen+1]();
  memcpy (msg,message.payload,message.payloadlen);
  Serial.print("  Payload: ");
  Serial.println(msg);

  JsonObject& root = deserializeJSON(msg);

  if (!root.success()) {
    Serial.println("\n*Error parsing the incoming JSON data");
    delete msg;
    return;
  }
  
  Serial.println("\nLoading desired state values");
  
  if (root["state"]["desired"]["volume"].is<int>()){
    speakerVolume = root["state"]["desired"]["volume"];
  } else{
    Serial.println("*The \"volume\" data received from the shadow was of the wrong type");
    delete msg;
    return;
  }
  
  if (root["state"]["desired"]["muted"].is<bool>()){
    muted = root["state"]["desired"]["muted"];
  } else{
    Serial.println("*The \"muted\" data received from the shadow was of the wrong type");
    delete msg;
    return;
  }

  //change to the new desired volume level, and send a success message
  if (configureMP3()){
    sendUpdateSuccess();
  }

  if (muted){
    Serial.println("\n --Shhhhh, the system is muted--");
  } else{
    Serial.println("\n --Ready to party!--");
  }
  
  //clean up the message
  delete msg;
}

//callback to handle messages from: "$aws/things/crazy-light/shadow/update/delta"
void updateShadowDeltaMessageHandler(MQTT::MessageData &md)
{
  Serial.println("\n----------------------------------------------------------------------------------------");
  MQTT::Message &message = md.message;
  
  char* topic = new char[md.topicName.lenstring.len+1]();
  memcpy (topic,md.topicName.lenstring.data,md.topicName.lenstring.len);
  Serial.println("\nDelta message received!");
  Serial.print("  Topic: ");
  Serial.println(topic);
  
  char* msg = new char[message.payloadlen+1]();
  memcpy (msg,message.payload,message.payloadlen);
  Serial.print("  Payload: ");
  Serial.println(msg);

  JsonObject& root = deserializeJSON(msg);

  if (!root.success()) {
    Serial.println("\n*Error parsing the incoming JSON data");
    delete msg;
    return;
  }
  
  Serial.println("\nDelta key found, loading new delta value(s)");
  if (containsNestedKey(root["state"], "volume")){
    if (root["state"]["volume"].is<int>()){
      speakerVolume = root["state"]["volume"];
    } else{
      Serial.println("\n*The \"volume\" data received from the shadow was of the wrong type");
      delete msg;
      return;
    }
  }
  
  if (containsNestedKey(root["state"], "muted")){
    if (root["state"]["muted"].is<bool>()){
      muted = root["state"]["muted"];
    } else{
      Serial.println("\n*The \"muted\" data received from the shadow was of the wrong type");
      delete msg;
      return;
    }
  }
  
  //change to the new desired volume level, and send a success message
  if (configureMP3()){
    sendUpdateSuccess();
  }

  if (muted){
    Serial.println("\n --Shhhhh, the system is muted--");
  } else{
    Serial.println("\n --Ready to party!--");
  }
    
  //clean up the message
  delete msg;
}

//callback to handle messages from: "party-time"
void partyTimeMessageHandler(MQTT::MessageData &md)
{
  Serial.println("\n----------------------------------------------------------------------------------------");
  MQTT::Message &message = md.message;
  
  char* topic = new char[md.topicName.lenstring.len+1]();
  memcpy (topic,md.topicName.lenstring.data,md.topicName.lenstring.len);
  Serial.println("\nParty-Time message received!");
  Serial.print("  Topic: ");
  Serial.println(topic);
  
  char* msg = new char[message.payloadlen+1]();
  memcpy (msg,message.payload,message.payloadlen);
  Serial.print("  Payload: ");
  Serial.println(msg);
  Serial.println();

  if (configureMP3()){ 
    partyTime();        
  }

  //clean up the message
  delete msg;
}
  
//---------------------Deserialize Incoming JSON----------------------//
JsonObject& deserializeJSON(char* json){  
  const size_t bufferSize = 3*JSON_OBJECT_SIZE(1) + 4*JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(4) + 190;
  DynamicJsonBuffer jsonBuffer(bufferSize);
  JsonObject& root = jsonBuffer.parseObject(json);
  return root;
}

//helper function for topic searching
bool containsNestedKey(const JsonObject& obj, const char* key) {
  for (const JsonPair& pair : obj) {
    if (!strcmp(pair.key, key))
      return true;

    if (containsNestedKey(pair.value.as<JsonObject>(), key)) 
      return true;
  }
  return false;
}

//--------------------------WiFi Connect---------------------------//
void wifiConnect() {
  int attemptCounter = 0;
  WiFi.begin(wifi_ssid, wifi_password);
  
  while (WiFi.status() != 3) {
    wifiStatus = WiFi.status();
    
    if (attemptCounter >= 15 && wifiStatus !=3) {
      wifiStatus = 4;
    }
    attemptCounter ++;
    
    switch(wifiStatus) {
      case 0:  //WL_IDLE_STATUS
        connectingBlinkYellow();
        Serial.println("Wifi Idle");
        break;
      case 1:  //WL_NO_SSID_AVAIL
        errorBlinkRed();
        Serial.println("No SSID Available");
        break;
      case 4:  //WL_CONNECT_FAILED
        errorBlinkRed();
        Serial.println("Wifi Connection Failed");
        break;
      case 6:  //WL_DISCONNECTED
        connectingBlinkYellow();
        Serial.println("Connecting to wifi...");
        break;
    }
  }        
  Serial.print("Wifi Connected: ");
  Serial.println(WiFi.localIP());  

  //after successful wifi connection, turn LED solid yellow
  digitalWrite(RED_LED, HIGH);
  digitalWrite(GREEN_LED, HIGH);
}

//------------------------------MP3 Module------------------------------//
class Mp3Notify
{
public:
  static void OnError(uint16_t errorCode)
  {
    Serial.print("\nCom Error ");
    Serial.println(errorCode);
    errorSolidRed();  
  }

  static void OnPlayFinished(uint16_t globalTrack)
  {
    Serial.print("\nPlay finished for #");
    Serial.println(globalTrack);   
  }

  static void OnCardOnline(uint16_t code)
  {
    Serial.print("\nCard online ");
    Serial.println(code);     
  }

  static void OnCardInserted(uint16_t code)
  {
    Serial.print("\nCard inserted ");
    Serial.println(code); 
  }

  static void OnCardRemoved(uint16_t code)
  {
    Serial.print("\nCard removed ");
    Serial.println(code);  
  }
};

//instantiate a DFMiniMp3 object 
SoftwareSerial secondarySerial(D2, D3); // RX, TX
DFMiniMp3<SoftwareSerial, Mp3Notify> mp3(secondarySerial);

void partyTimeLights(uint16_t msWait)
{
  uint32_t start = millis();
  
  while ((millis() - start) < msWait)
  {
    partyTimeLED();  //crazy freak out LED when music plays
    mp3.loop();  //calling mp3.loop() periodically allows for notifications to be handled without interrupts
    delay(50);

    unsigned long currentMillis = millis();
    if (currentMillis - previousMillis >= 300) {
      //save the last time you blinked the LED
      previousMillis = currentMillis;
  
      if (pinState == LOW) {
        pinState = HIGH;
      } else {
        pinState = LOW;
      }
  
      digitalWrite(RELAY_LIGHT, pinState);
    }
  }
}

//--------------------------MP3 Module Config------------------------//
bool configureMP3(){
  int16_t tempVolume;
  
  if (speakerVolume > 25){
    Serial.println("*The volume level received is above the max limit... The max value will be used");
    tempVolume = 25;
  } 
  else if (speakerVolume < 0){
    Serial.println("*The volume level received is below the min limit... The min value will be used");
    tempVolume = 0;
  }
  else{
    tempVolume = speakerVolume;
  }
  
  mp3.begin();
  Serial.print("Setting volume (0-25) to: ");
  Serial.println(tempVolume);
  mp3.setVolume(tempVolume);
  Serial.print("Setting mute to: ");
  Serial.print(muted ? "true" : "false");

  if (tempVolume != mp3.getVolume()){  //getVolume() will return 0 if a comm error exists
    errorSolidRed();
    Serial.println("\nError communicating to MP3 module");
    return false;
  } else{
    readyFlashBlue();
    Serial.println("\nConfig was successful");
    return true;
  }
}

//--------------------------LED status codes-------------------------//
void mutedBlinkBlue(){
  digitalWrite(GREEN_LED, LOW); 
  digitalWrite(RED_LED, LOW);
  digitalWrite(BLUE_LED, HIGH);
  delay(1000);
  digitalWrite(BLUE_LED, LOW);
}

void connectingBlinkYellow(){
  digitalWrite(BLUE_LED, LOW); 
  digitalWrite(RED_LED, HIGH);
  digitalWrite(GREEN_LED, HIGH);
  delay(500);
  digitalWrite(RED_LED, LOW);
  digitalWrite(GREEN_LED, LOW);
  delay(500);
}

void readyFlashBlue(){
  digitalWrite(RED_LED, LOW);
  digitalWrite(GREEN_LED, LOW);
  digitalWrite(BLUE_LED, HIGH);
  delay(60);
  digitalWrite(BLUE_LED, LOW);
  delay(60);
  digitalWrite(BLUE_LED, HIGH);
  delay(60);
  digitalWrite(BLUE_LED, LOW);
  delay(60);
  digitalWrite(BLUE_LED, HIGH);
  delay(60);
  digitalWrite(BLUE_LED, LOW);
  delay(60);
  digitalWrite(BLUE_LED, HIGH);
}

void errorBlinkRed(){
  digitalWrite(BLUE_LED, LOW);
  digitalWrite(GREEN_LED, LOW);
  digitalWrite(RED_LED, HIGH);
  delay(200);
  digitalWrite(RED_LED, LOW);
  delay(200);
}

void errorSolidRed(){
  digitalWrite(BLUE_LED, LOW);
  digitalWrite(GREEN_LED, LOW);
  digitalWrite(RED_LED, HIGH);
}

void readySolidBlue(){
  digitalWrite(RED_LED, LOW);
  digitalWrite(GREEN_LED, LOW);
  digitalWrite(BLUE_LED, HIGH);
}

void partyTimeLED(){
  digitalWrite(BLUE_LED, random(0, 2));
  digitalWrite(GREEN_LED, random(0, 2));
  digitalWrite(RED_LED, random(0, 2));
}

//-----------------------------Party Time------------------------------//
void partyTime(){
  if (muted){
    Serial.println("\n --System is muted... No party for YOU!--");
    return;
  }
  Serial.println("\n ﾟ｡･*.ﾟ☆ PARTY TIME ☆ﾟ.*･｡ﾟ"); 
  mp3.playRandomTrackFromAll();
  digitalWrite(RELAY_MOTOR, HIGH);  
  partyTimeLights(10000);
  digitalWrite(RELAY_LIGHT, LOW);
  digitalWrite(RELAY_MOTOR, LOW);  
  mp3.stop();
  readySolidBlue();
}

//------------------------------Setup-----------------------------//
void setup() {
  Serial.begin (115200);

  //initialize digital pins as outputs.
  pinMode(RELAY_LIGHT, OUTPUT);  // light relay
  pinMode(RELAY_MOTOR, OUTPUT);  // motor relay
  pinMode(RED_LED, OUTPUT);      // red LED
  pinMode(BLUE_LED, OUTPUT);     // blue LED
  pinMode(GREEN_LED, OUTPUT);    // green LED

  //begin wifi connection
  wifiConnect();

  // fill AWS parameters    
  awsWSclient.setAWSRegion(aws_region);
  awsWSclient.setAWSDomain(aws_endpoint);
  awsWSclient.setAWSKeyID(aws_key);
  awsWSclient.setAWSSecretKey(aws_secret);
  awsWSclient.setUseSSL(true);

  //connect to websocket layer & subscribe to MQTT topic
  if (connect()){
    subscribe(aws_get_shadow_accepted_topic, getShadowAcceptedMessageHandler);
    subscribe(aws_update_shadow_delta_topic, updateShadowDeltaMessageHandler);
    subscribe(aws_party_time_topic, partyTimeMessageHandler);
  }

  //get the initial state of the AWS shadow document by sending an empty payload
  sendMessage(aws_get_shadow_topic, "{}");
}

//----------------------------Main Loop-----------------------------//
void loop() {
  //keep the mqtt up and running
  if (awsWSclient.connected()) {    
      client->yield();
  } else {
    //handle reconnection
    if (connect()){
      subscribe(aws_get_shadow_accepted_topic, getShadowAcceptedMessageHandler);
      subscribe(aws_update_shadow_delta_topic, updateShadowDeltaMessageHandler);
      subscribe(aws_party_time_topic, partyTimeMessageHandler);
    }
  }

  //slowly blink the indicator light while the system is muted
  if (muted){
    mutedBlinkBlue();
  }
}
