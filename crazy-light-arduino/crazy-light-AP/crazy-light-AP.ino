#include "ESP_WifiConfig.h"
#include <Adafruit_NeoPixel.h>

#define RED_LED D6
#define BLUE_LED D8
#define buttonPin D4

int buttonState = 0;         // variable for reading the pushbutton status

String WIFI_SSID = "EXCELION"; 
String WIFI_PASSWORD = "thevault!";

bool isConnected;
ESP_WifiConfig wifi;
ESP8266WebServer server(80);

const byte DNS_PORT = 53;
IPAddress apIP(192, 168, 4, 1);
DNSServer dnsServer;

//-----------------------------------------------------//
void setup() {
  Serial.begin(115200);

  pinMode(RED_LED, OUTPUT);      // red LED
  pinMode(BLUE_LED, OUTPUT);     // blue LED
  pinMode(buttonPin, INPUT_PULLUP);
  
//  WiFi.disconnect(true);  // clears previously stored wifi credentials
//  startAP();
  wifiConnect();
}

//-----------------------------------------------------//
void loop() {
  buttonState = digitalRead(buttonPin);
  dnsServer.processNextRequest();
  server.handleClient();

  if (buttonState == LOW) {
    startAP();
    delay(2000);
  }
//  Serial.print(wifiStatus);
//   if (WiFi.status() != 0) {
//    Serial.println("Wifi Connection Failed");
//    wifiConnect();
//  }

}

//-----------------------------------------------------//
int wifiStatus;
void wifiConnect() {
  int attemptCounter = 0;
//  WiFi.config(ip, dns, gateway, subnet); <--- Use this to set a static IP in STA mode
  WiFi.begin(WIFI_SSID.c_str(), WIFI_PASSWORD.c_str());
  
  while (WiFi.status() != 3) {
    int wifiStatus = WiFi.status();
    
    if (attemptCounter >= 15 && wifiStatus !=3) {
      wifiStatus = 4;
    }
    attemptCounter ++;
    
    switch(wifiStatus) {
      case 0:  //WL_IDLE_STATUS
        connectingBlinkBlue();
        Serial.println("Wifi Idle");
        break;
      case 1:  //WL_NO_SSID_AVAIL
        errorBlinkRed();
        Serial.println("No SSID Available");
        break;
      case 4:  //WL_CONNECT_FAILED
        errorBlinkRed();
        Serial.println("Wifi Connection Failed");
        break;
      case 6:  //WL_DISCONNECTED
        connectingBlinkBlue();
        Serial.println("Connecting to wifi...");
        break;
    }
  }
  readySolidBlue();
  isConnected = true;        
  Serial.print("Wifi Connected: ");
  Serial.println(WiFi.localIP());
  WiFi.disconnect(true);
}

//-----------------------------------------------------//
void startAP(){
  Serial.println("Starting access point");
  
  WiFi.mode(WIFI_AP);
  WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
  WiFi.softAP("Crazy-Light Wifi Setup");
  //WiFi.softAP(ESP_WIFICONFIG_ACCESS_POINT_USER, ESP_WIFICONFIG_ACCESS_POINT_PASS);
  //this->IpAddress = WiFi.softAPIP();


  // if DNSServer is started with "*" for domain name, it will reply with
  // provided IP to all DNS request
  dnsServer.start(DNS_PORT, "*", apIP);

  // replay to all requests with same HTML
  server.onNotFound([]() {
    String container = "<html>"
                  "<head>"
                    "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
                    "<title>Party-Light WiFi Setup</title>"
                    "<style>"
                      ".container{padding: 16px;}"
                      ".myLabel{font-weight: bold;}"
                      "select,input{display: block;width: 100%}"
                      ".center{text-align: center;}"
                      ".myText{font-size: 1.25em;}"
                      ".myButton {"
                         "background: #D0021B;"
                         "color: white;"
                         "border: none;" 
                         "height: 50px;"
                         "width: 200px;"
                         "border-radius: 25px;"
                         "font-size: 1.25em;"
                      "}"
                    "</style>"
                  "</head>"
                  "<body>" 
                  
                  "<div class='center'><img src='data:image/jpeg;base64,/9j/4AAQSkZJRgABAQIAJQAlAAD//gAyUHJvY2Vzc2VkIEJ5IGVCYXkgd2l0aCBJbWFnZU1hZ2ljaywgejEuMS4wLiB8fEIx/9sAQwAGBAUGBQQGBgUGBwcGCAoQCgoJCQoUDg8MEBcUGBgXFBYWGh0lHxobIxwWFiAsICMmJykqKRkfLTAtKDAlKCko/9sAQwEHBwcKCAoTCgoTKBoWGigoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgo/8IAEQgAlgCWAwEiAAIRAQMRAf/EABsAAQACAwEBAAAAAAAAAAAAAAAFBgMEBwIB/8QAGgEBAAMBAQEAAAAAAAAAAAAAAAIDBAUBBv/aAAwDAQACEAMQAAAB6oAABB7fK+jnsyqOvmtaqPVrVQWvYpiLtHqEm/m94QkAAx5Oe6K4mNPqecEvAAE7k6NytL2cHaAAIyXkVz33q/Tc/OwNFedgzyGH5FnstfuWC+4+qfm42q1KvYoe5hVMDFyqVgu3jluieq3ydVig7JA1Slue9F92R0Y2diK5S1Xt3NLYXShXqo3xrtwqfSOvlnh85vVS18x3UwPU6Da7I2iL5fNVS6dB6kTltsfNrLUOjn6tjh9bnaL5yy1833UymLf6Rrp05Y4uwI+tPce+VpZV8K18s1fs8pmvh9dbLmxbGOuWH1me+adklbXms8+jk6gAAAPPJ7bQu5jDr5RKw90+j7u789uDBeAAAAwZ6LfCr659XzT7d6ZRfQsj5veGewAAAADS5L07l3dxM+10nRDRnj53cEJAAAAAAI4l5u5CPoAAAAAAH//EACkQAAICAgEDBAIBBQAAAAAAAAMEAgUAARMRFTAQEhQgBjIhJDEzNUD/2gAIAQEAAQUC+9jYQUzvZ872fO9nzvZ872fO9nzvZ8Bd798d6lHwPNRVAWcik8FN17d9yTiOD7Umz+CtQk3KEdQj97l3mn4Kyv21uMdQj97p3hh4Kuu2xmta1r7vtRUAUm5S5hZzCzmFnML02Uet8wsVYTiXX5AlrNfkCm997BmrpfqIkSw9SkiIbzMmz0qXyz8I84R5WDhInCLGKY0XV0wADdDHGq4h4a1SEcUU317VOKLWfjpJcnrcu85Fl5uMBHAIln9tXWVH6FnEQ+uugiRKO6/1+HqnZN1Ke0lfyA8TOwjKcqlL4g/S7d4ob65VJaSWJHU4JVqyUspv5Tut9Kn5TGkafXSqt/8ABkrR7cyOul0mrM80EhqQ9bqEo2FOyoApL1TUJEMSVO+NMnfEsq7ZUCNrbLM15P0TuExKP2yps74lgv0QQI3JcA1x/RlYTMezLZ2ZbOzLZ2ZbNrjIeS8oK/HjzwDrY+LWoyB0IWHHKtq9lyMdRj4blrgWETjHy/0mzC+UM/QOjx6QN1frqz2b8Ut6jp5jbTP0AEjBK9AamvHfNewf0RSI3NVYaw/GYkQiOWRzetbWSYwcIjh5L9r3T9P77rar2+Z0+llpS3KWBFMxK6ugrrzW4ZHRxNQjc01Bqj/4CorFmOERx83/xAAjEQACAgEDBAMBAAAAAAAAAAABAgARAxIhMRATIFEEIjAy/9oACAEDAQE/AeubLo2E7z+53n9zvP7nef3FNi/DI+gXCbNnwxYb3bwJreOxyG5pMqUZiQctO4oiuG465GORtCxVCCh0Khuej2B9Zl/jeYFOq+mdiF2mFAgsxcysaliZshFVLEzPpH1iI2XduIABsOlXO0nqMiKLqDRUpJSRMG9ny+Q9/XoATsJiwhNz5ZH0C5zEQvxExhOPP5N3MeIvvAoUUPwIB5/H/8QAKhEAAgEDAQcEAgMAAAAAAAAAAQIDAAQSEQUQEyAhMVEUIkFSIzAykfD/2gAIAQIBAT8B32Nnxjk/avQwfWvQwfWvQwfWjYQEaY1IuDlfHJbQGd8RSIEXFeS9v+H+OPvyKpc4rVtClsmPzXEXzQYHtXEXzV5cPphD/dC0mPZakgki/mNN9pCttHx5e9TStM5c7o5WiOqHdCsbN+Q6CrMkXACVtGRFiKnud2z41km91X0zzvio6CpbGWJMjXDfxVjbBwxkHauG3irC2EjkSCp54bT2wj3U7s5ybcCVOor1c/2NRz3EjBFY0yzBtF7aii1xj01rK41Ov+7VPtA44Ievnm2Zb6Din53O6xjJqu75pvavRea2hM8gSgAo0FXFwkC6tVxcvcHVufZQXAn5q7vVg6Dq1SSNI2TfoVivVT+n/8QAOhAAAgECAgUKAwYHAQAAAAAAAQIDABESIQQTMUFRECAiIzAyM3GRoUJSYRRzkqLR4TRAU4GTscGy/9oACAEBAAY/AufhtjlPw14UfvXhR+9eFH714UfvXhR+9eFH714UfvXXxgLxXdQIzB7EudvwjiaZ3N2bb2MWL629ewLubKMzWM5KMlHY4nyhG/jQVRYDIdhqYz1a7fqex1kmUP8A6oKosBu7DUxnrG2/QdiJZsotw+agALDsMZzbYo400kjZnMmvET1rxE9a8RPWvET15LF1B868RPWsWkviUfCpGdb/AGrLEfSvDl9qzWUf2oPGwZTv5jO5so20XOQ+EcBWulHURnIfO37V4aeleGnpWnEov8QRs+grw0/DWq0bLRnzx/0xw/SljSNcI4itJsig4bbK7i+lPEYJCUOEkR5UHWOKSM8UpBFfUyg2HykcksXw2xczVRnql9zSwR5b3b5VpY4xhRRYCnhjPURRnP5muOTSTx0iT/dM7myqLk1fdQeM4kOw0w4sg/OOSdkSLC8hYEvWB2DOzY2I2XqKJDfUg4vM7qCoCWO4US/itt+nLqIz022ngKCoMTtkqjeaw7ZWzduJplN7EWyoto6FSRbNieQnjLIfzmtL+7NfY79R8188Py1on3S/6qJfmnjH5hyPhnULjYAascatJpT24IAtauBf2rLOQ7W5khbY1iKafSS+t7qLqycI40xj1rvuXVkXovLNLjbM4XIFaR9pkmIOHD3n413pP8TVHHKZA+ZPVtvN6mihMhdxYdWRTeVQRs0l1QKerbhWj4DJ0ZldurbYK70v+Jqud9zV+7FvasES2HNwzLf/AJW2T8VbZPWtsn4q2yfirSV0fWMsaEpbMk5U0sqSI2MKARa+VCAy2lvh7vRvwvSs7FcUmrsFv/2pmZ8o2C9EXvt/SpUxglFxDLbQUnpWuRw+lCTSARHuXeaAUWA3dlhU9ZJkKnXO7rhFvOtVni1usvu2V9oAkxYseAgWxefClXWTIwcscG+9vrU9scOscMNXu25bRUUqo2TLkO8371rtK6Upzw8OzJOQFNJ8OxfLm4Iluav3pd7doNHXvP3vLm9HJBtasEQ8zx7RpH7qi9PI+1uYJJrrF7tQVAAo3DtRo67Bm3LYbaEulDPcn69s8p3bPOizG7HM8gSNcTGsTdKbjw8u3dUzYdK3JaPu724VhjGe9t5/kcTwqW41hRQq8B2//8QAKRABAAEDAgQGAwEBAAAAAAAAAREAITFBUTBhcfAQIJGh0fGBscHhQP/aAAgBAQABPyHzwogJJRBu8AQhCEISAKdd9lKGCkTU4N5vGklDKVwe9ESjgCYLI6Upk6Inzwb5GX1cp80FgcBocCfvu87oOCdFP16OXOjhGgGDgaLu47peC4BHqf4o2QEAacBxAnUqkVJLr6rX1WvqtfVazTIcyJr6rRAF0XBzvihAIhpPzqLFth+dfSfKmAE3h81j4EPIAYspqyW2pMpb+wH8/bpX02vr9Lp20nQ6WIfTUEvGKvvc3bSoE5iQV5rvSi8iQ62r6vSqgi2RU0dS1Z1ESzTqyh2bCQ5X8ATL7WzMd9PJq2bp3QUxSEg90uChfhC0K0FWMRE9C56+B6/+kf5Up7naBmhiUUTM2ij3hkcJV/8Av8fBNqNvZbWikTWARJFjlYoYUozSL2B71ZaoZGlTjDZgbeK2Edrulq4ygzCYKlxGx/Y5GClbClKH1qb1TgNrviXmf8v1IqaNrsunPa1QfneyrvvFfzwXy0gIBB+qdkPPuEJ96Ac6unMtaKy/l6bHkkYgXuRH8o00SLfGGX9VCwDWTaUtTPDWGcgHFGGaQwXbxkrvz+UOGJBsuznRXCE6o1SpWsyih4TzWAbVDVmkKR26V3x/KmkEVc5q1pJN3XkURIfqu75YCSYcPQ13r4rvfxXeviu9fFA+JFhI2uZqfgbcElsnKi8JwTHWTm0xTAZBa0CdG9GUvN/USXLUga19ogpyQW3JpyFhhRNv+URWrssHV2KLMaAEBwsFvwBq0cBHposWfwURiLILQR/dSzFrEMterlVzwcSIuRs6OahLtq2B+wqZLuGCy9V6xiy9cTq7vDUsBKulXxaDbyjLH9A3a1Si4Y5G3EmTqcm38+WLXa+WOm7QeCamVu8RzIkKzlU9OXk3AMMfAUA1IMBxZD/t9DxBAFVgNaCEOeidtKOLdAjZvoqb0ZN3wQNoiiEbHTyfLjiCoANY8IrQM+P9VkyZv+EkWGcJ6xQ0QwEHH//aAAwDAQACAAMAAAAQ888tOMMfd888h9//AP8A7zzzxLPTv77XzxNbw2A8KDzynmTsCxv7z09i0ncVzzzzzb/t7zzzzzrz3fzzzzzw2V3zzzzzzyB7zzzzzzz/xAAjEQEAAgEEAQUBAQAAAAAAAAABABEhECAxYUEwUbHR8JGh/9oACAEDAQE/ENaHl2AAJuKL3bBtR25NnXvnYBVxMdYnRFHJOqW/844yzkt6mOD98QhpEUL0VwWwCn9Ywjg07GlhMsoKd0RD5ud0cVZuZJg2ijRApnQiRjESC80/vqFua8/yYQPf7gNPHg+916njnR2i2eQHcCwqrYtUF1y3qk8Ry2CVAx6GAFwK49D/xAAoEQEAAQIEBQQDAQAAAAAAAAABEQAhMUFRYRBxkaHRIIHB8DCx8eH/2gAIAQIBAT8Q4lFW93xrWz7+a2ffzWz7+abWPd81DbMk6PoH4WbofcKLFAegpLvdP9/VLN3idCVoE0lddXxpWz6lY2GkbI6lWolcZFuV8d8ueFxRoAVDxR2EW2PL9zr+GZwSQFI4Ri2Ymvem1YXqb+1bZYe+PAYYQmNU+zUwGVZu6+KCRIxYlf1W56NY/EJ37Vu+jVomGcl5KlSy7c34pGkrwMPCV/aqfid/uFJ0UDeZYLukLjUQvJMmd5iDDDXnRKyIJwtILTNks9LUUzs3wPP9pZu+mfO9hy19/uPBysBSr8o8/HqEGGfLOjBQFSrvkZtTksYGR919Zh5oeWXzQvtmnPxSZ5X8C8gdmKVWX8H/xAApEAEAAQMDAAsBAQEAAAAAAAABEQAhMUFRYRAgMHGBkaHB0fDxseFA/9oACAEBAAE/EOuFawEGkuhsZfWv2PlX7Hyr9j5V+x8q/Y+VfsfKv2PlTD4hQeSsncz30a8D0iEieHYx6vmXisdxldqWmy3V2NgwGx2Kd5YEvsR6dgys+CBV5cFb+Xll8tOxKFHDsn3nR30IZoUAEAddYq8ncqx6c+o30OxeVPcpps3eA3Cs2NgDAHYS8O913/mA2JdqLHYIzKUsvtz103okkg4AYA0OwJ25t+oZX5o6SzMEuvBX0b3r6N719G96+he9CARkbjVlsAxO8Wvo3vVzAgRsNHgM0tZUAIDYqSdcKPRGrxbduHgSoLwy2z8PHUaCfSg9+KGNuq/Rcv8AhRER4LyUat4PfV939q+p+1BA7gII4tiRpQkJCMKeVAoKLZGNyv5obJqzfop8qCVMq7tMRmQIsEW5qz9zyrWlPCVCMudqsQmJAMSADow0YQp+ekm8woKwjpHROYgOgQk7xKGOhqRk3esevOA3b7Vzs1YQp5RvLgaxi6rAg7+/WnBXFJk0Otk3ZXt0Sc5N5axz4UBpT3A1EjpIVCZnas32Syok3OaPdyb/ACHQ0ANOXkGlop5g6gAQt4AS3YW0xRslZyQRLdCmkKd08NKVukNYMF1yq/HSidKoutoPE7idylxDliB7ugLpQA3DNOI4VjYnK0iSwchIYFxvkuUFUbWeyAaCduh2f3n4q5A8++6rd02bFr5bpot3H9Te9PoZs13TGLhb2FWipSYbfzaDatdyGt/1LzUcNoH4A9DznqElJBgCo7lFLHkgltIM07Ys1aKlC8B0QC5XBT4xTjOjAGDgopj27slHwDih2nxMDWDkiLQg/j5maQBac1JduMLrDFGUvDAoJ5lFy6YEpG998C9CpQ2kDoITKHjUQTBA8jq84PSrqolzujV56pFrTMq3C5TtvEKfl6flqP8AjqNIAjQQJIthdKNzDcooAVkFqIzGRLdh7oIMHfNSbeJ1KXBJIBbNQm4ZyUSHlc3kqS6AKRuO30brlpNQMkq1rjNwpWIVNKmqcJkO+r1HijS0FANAOyn4gYm/x7By8VepPwG/AxJiaGbzESEEzMynERrUzMyQmYmi/FS1s0TFvnCLtpNYKTpUTEbQTJbWkxQN5aAgiwgYU3i7dzQmaUJPJU3XuOc0W7JSZHoAEq07USl6ePFuvL1bwpnTdGhRNxDeKB0errtR2eAMtuBt4h5Dv1YUsZLcA9DTWsSCUTvjX+GnaTg6XVjQ5Wxy0yd+yZDgHAQeHSsEtJH4DHH9Mum9Q13JAdo1kjxTWPpjLym3S4hQCVOADLWuG1Dsu58hrOggjtQUFAuswPNPCaRIynKMr0YNUaRqrgDdrJviGXIP+njFHbZJJqTyhzExz0X3aRbfvwPSg8yBuV3XbYLFR/wNHZICn3hPjWHdph4Hb//Z'/></div>"
                  "<h1 style='color:#D0021B;' class='center'>Crazy-Light WiFi Setup</h1>"
                    "<form method='post' action='config'>"
                      "<div class='container'>"
                        "<label class='myLabel myText' for='ssid'>WiFi Network:</label>"
                        "<select id='ssid' class='myText' name='ssid'><option>Select WiFi network</option></select>"
                      "</div>"
                      "<div class='container'>"
                        "<label class='myLabel myText' for='password'>Password:</label>"
                        "<input id='password' class='myText' type='password' name='password' placeholder='Enter network password'>"
                      "</div>"
                      "<div class='center'><button class='myButton' type='submit'>Save</button></div>"
                    "</form>"
                    "<script>"
                      "var list = %LIST%;"
                      "function addToSelect(item, index){"
                        "var optionElement = document.createElement('option');"
                        "optionElement.text = item;"
                        "document.querySelector('select').add(optionElement, index + 1);"
                       "}"
                       "list.networks.forEach(addToSelect);"
                      "</script>"
                  "</body>"
                "</html>";
    String page = container;
    page.replace("%LIST%", wifi.nearAccessPoints());
    server.send(200, "text/html", page);
  });
 // server.on("/", loginUI);
  server.on("/config", configHandler);
  server.begin();

//  
//  server.on("/", loginUI);
//  server.on("/config", configHandler);
//  server.begin();
//  Serial.print("Serving login webpage at: ");
//  Serial.println(WiFi.softAPIP());
//  Serial.println("Please use UI to enter local SSID & PW");
}

//-----------------------------------------------------//
//Get user's login info and store it in memory
void configHandler(){
  String ssid;
  String password;
  
  for(uint8_t i = 0; i < server.args(); i++){
    if(server.argName(i) == "ssid"){
      ssid = server.arg(i);
    }
    if(server.argName(i) == "password"){
      password = server.arg(i);
    }
  }
  WIFI_SSID = ssid.c_str();
  WIFI_PASSWORD = password.c_str();
  
  server.send(200, "application/json", "{\"success\":\"true\",\"message\":\"Information saved\"}");
  
  Serial.println("WiFi credentials accepted");
  wifiConnect(); 
}

//-----------------------------------------------------//
//Create WIFI login UI
//void loginUI() {
  
//    String page = container;
//    page.replace("%LIST%", wifi.nearAccessPoints());
//    server.send(200, "text/html", page);
//}

//--------------------------LED status codes-------------------------//
void connectingBlinkBlue(){
  digitalWrite(BLUE_LED, LOW); 
  digitalWrite(RED_LED, LOW);
  delay(500);
  digitalWrite(BLUE_LED, HIGH);
  delay(500);
}

void errorBlinkRed(){
  digitalWrite(BLUE_LED, LOW);
  digitalWrite(RED_LED, HIGH);
  delay(200);
  digitalWrite(RED_LED, LOW);
  delay(200);
}

void readySolidBlue(){
  digitalWrite(RED_LED, LOW);
  digitalWrite(BLUE_LED, HIGH);
}


