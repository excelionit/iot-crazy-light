#ifndef _ESP_WIFICONFIG_H
#define _ESP_WIFICONFIG_H

#ifndef ESP_WIFICONFIG_ACCESS_POINT_USER
#define ESP_WIFICONFIG_ACCESS_POINT_USER "Party Light Wifi Setup"
#endif

#ifndef ESP_WIFICONFIG_ACCESS_POINT_PASS
#define ESP_WIFICONFIG_ACCESS_POINT_PASS ""
#endif

#include <ESP8266WiFi.h>
#include <Adafruit_ADS1015.h>
#include <ESP8266WebServer.h>
#include "Arduino.h"
#include <WiFiClient.h>
#include <DNSServer.h>

class ESP_WifiConfig{
public:

    ESP_WifiConfig(){};

    bool wifiConnect(String ssid, String password);
   // bool startAccessPoint();
    String nearAccessPoints();

    void resetWifi();

private:

    String SSID_STR;
    String PASSWORD;
    IPAddress IpAddress;
};

#endif //_ESP_WIFICONFIG_H
