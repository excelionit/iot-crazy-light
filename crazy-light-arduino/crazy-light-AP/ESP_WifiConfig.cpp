#include "ESP_WifiConfig.h"

//bool ESP_WifiConfig::startAccessPoint(){
//  WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
//  WiFi.softAP(ESP_WIFICONFIG_ACCESS_POINT_USER, ESP_WIFICONFIG_ACCESS_POINT_PASS);
//  //this->IpAddress = WiFi.softAPIP();
//}

String ESP_WifiConfig::nearAccessPoints(){
  String networkList = "{\"networks\":[";
  int n = WiFi.scanNetworks();
  if(n == 0){
    networkList += "]}";
  }else{
    for(int i = 0; i < n; i++){
      networkList += "\"" + WiFi.SSID(i) + "\"";
      if(n > i+1){ 
        networkList += ",";
      }
    }
    networkList += "]}";
  }
  return networkList;
}
