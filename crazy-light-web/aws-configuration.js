var awsConfiguration = {
   poolId: 'us-east-1:356660c8-7831-42b0-9bda-d2543e8f37a4', // 'YourCognitoIdentityPoolId'
   host: 'ax6j6875rjidk-ats.iot.us-east-1.amazonaws.com', // 'YourAwsIoTEndpoint', e.g. 'prefix.iot.us-east-1.amazonaws.com'
   region: 'us-east-1' // 'YourAwsRegion', e.g. 'us-east-1'
};

module.exports = awsConfiguration;