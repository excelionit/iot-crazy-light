'use strict';

module.exports.party = (event, context, callback) => {
  try {
    console.log(`event: ${JSON.stringify(event)}`);
    console.log(`context: ${JSON.stringify(context)}`);

    startTheParty();

    const response = {
      statusCode: 200,
      body: JSON.stringify({
        message: 'Can haz start party',
        // input: event,
      }),
    };

    callback(null, response);
  } catch (error) {
    console.error(error);
    callback(error);
  }
};

const AWS = require('aws-sdk');
const endpointUrl = 'ax6j6875rjidk-ats.iot.us-east-1.amazonaws.com';

const startTheParty = function () {
  console.log('Starting the party!!');

  console.log('Creating IoTData service object.');
  var iotdata = new AWS.IotData({
    endpoint: endpointUrl,
    region: 'us-east-1',
  });

  console.log('Creating parameters.');
  var params = {
    topic: 'party-time',
    payload: new Buffer(''),
    qos: 0
  };

  console.log('Publishing IoT message...');
  iotdata.publish(params, function (err, data) {
    console.log('Published IoT message.');
    if (err) console.log(err, err.stack);
    else console.log(data);

    console.log('The party has started!!');
  });
};

module.exports.startTheParty = startTheParty;
