'use strict';

const AWS = require('aws-sdk');

var iotdata = new AWS.IotData({endpoint: 'ax6j6875rjidk-ats.iot.us-east-1.amazonaws.com'});

exports.handler = (event, context, callback) => {
  if (event.clickType === 'SINGLE') {
    console.log('---> SINGLE PRESS <---');
    
    var params = {
      topic: 'party-time',
      payload: new Buffer(''),
      qos: 0
    };
    
    iotdata.publish(params, function(err, data) {
      if (err) console.log(err, err.stack);
      else     console.log(data);      
    });
    
    
  } else if (event.clickType === 'DOUBLE') {
    console.log('---> DOUBLE PRESS <---');
    
    var getParams = {
      thingName: 'crazy-light'
    };
    
    iotdata.getThingShadow(getParams, function(err, data) {
      if (err) console.log(err, err.stack);
      else{
        
        var payloadObject = JSON.parse(data.payload);
        console.log(payloadObject);   
        
        if (payloadObject.state.reported.muted == false){
          var setParams = {
            payload: new Buffer('{"state": {"desired": {"muted": true}}}'),
            thingName: 'crazy-light'
          };
          iotdata.updateThingShadow(setParams, function(err, data) {
            if (err) console.log(err, err.stack); 
            else     console.log(data);        
          });
        } 
        else{
          var setParams = {
            payload: new Buffer('{"state": {"desired": {"muted": false}}}'),
            thingName: 'crazy-light'
          };
          iotdata.updateThingShadow(setParams, function(err, data) {
            if (err) console.log(err, err.stack); 
            else     console.log(data);        
          });
        }
      }
    });
    

//  } else if (event.clickType === 'LONG') {
//    console.log('---> LONG PRESS <---');
//    var params = {
//      payload: new Buffer('{"state": {"desired": {"muted": false}}}'),
//      thingName: 'crazy-light'
//    };
//    iotdata.updateThingShadow(params, function(err, data) {
//      if (err) console.log(err, err.stack); 
//      else     console.log(data);       
//    });
  }
};